class CreateParticipates < ActiveRecord::Migration
  def change
    create_table :participates do |t|
      t.references :event
      t.references :member

      t.timestamps
    end
    add_index :participates, :event_id
    add_index :participates, :member_id
  end
end
