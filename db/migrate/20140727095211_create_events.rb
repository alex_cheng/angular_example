class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.date :occured_at
      t.float :cost
      t.string :location
      t.string :description
      t.text :details

      t.timestamps
    end
  end
end
