var theApp = angular.module('TheApp', []);

// theApp.config(function ($route){
// 	$route
// 		.when('/',
// 		{
// 			controller: 'MemberController',
// 			templateUrl: 'Partials/Members.html'
// 		})
// 		.otherwise({ redirectTo: '/' });
// });

theApp.controller('MemberController', function ($scope, $http) {
	$scope.refresh = function () {
		$http.get('/members.json').success(function(data, status) {
			$scope.members = data;
		});
	};

	$scope.show = function (member) {
		alert(member.name);
	};

	$scope.addMember = function (newMember) {
		$http.post('/members', {
			member: newMember
		}).then(function(data, status) {
			$scope.refresh();
		});
	};

	$scope.delete = function (item) {
		$http.delete('/members/' + item.id + '.json').then(function(data, status) {
			$scope.refresh();
		});
	};

	$scope.refresh();
});